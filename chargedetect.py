#!/usr/bin/python

from time import sleep
import RPi.GPIO as GPIO


GPIO.setmode(GPIO.BCM)

# charging
GPIO.setup(23, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)

# charged
GPIO.setup(24, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)

# low battery
GPIO.setup(17, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)



if not GPIO.input(23) and not GPIO.input(24):
   exit(1)

if GPIO.input(23):
   exit(0)

if GPIO.input(24):
   exit(0)

if not GPIO.input(17):
   exit(2)







GPIO.cleanup()
