#!/usr/bin/python

from time import sleep
import RPi.GPIO as GPIO
from raspledstrip.ledstrip import *
from raspledstrip.animation import *
import os.path

cwd=os.getcwd()
if os.path.isfile(cwd + "/settings/noled"):
   print "File exists. Exiting.\n"
   exit()




num=8
led=LEDStrip(num)
led.all_off()


GPIO.setmode(GPIO.BCM)

GPIO.setup(23, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)

GPIO.setup(24, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)

GPIO.setup(17, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)


def charging(channel):
	# Filter out bounces of plugging and unplugging
	sleep(1)
	GPIO.remove_event_detect(24)
        while GPIO.input(23):
           print("Charging")
	   # Start the fade loop
           step = 0.01
           for c in range(1):
              r, g, b = (0,0,255)
              level = 0.01
              dir = step
              while level >= 0.0:
                 led.fill(Color(r, g, b, level))
                 led.update()
                 if(level >= 0.99):
                    dir = -step
                 level += dir
                 sleep(0.0075)
	   sleep(3)
	if GPIO.input(24):
           #print("setting it up")
	   led.all_off()
	   #GPIO.add_event_detect(24, GPIO.RISING)
           #GPIO.add_event_callback(24, charged)
           #break


def charged(channel):
	sleep(1)
	GPIO.remove_event_detect(23)
	while GPIO.input(24):

	   print("Charged")

           anim = RainbowCycle(led)
           for i in range(512*3):
              anim.step()
              led.update()
	#GPIO.add_event_detect(23, GPIO.RISING)
	#GPIO.add_event_callback(23, charging)
	led.all_off()


def lowbattery(channel):
	sleep(1)
	while not GPIO.input(17):
	
	   print("LOW BATTERY")
	              # Define our colors

           # Start the fade loop
           step = 0.01
           for c in range(1):
              r, g, b = (255,0,0)
              level = 0.01
              dir = step
              while level >= 0.0:
                 led.fill(Color(r, g, b, level))
                 led.update()
                 if(level >= 0.99):
                    dir = -step
                 level += dir
                 sleep(0.0075)


# Check for high signals on start of script in case it was plugged
# in while the system was booting.
#if GPIO.input(23):
#   charging(23)

#if GPIO.input(24):
#   charged(24)

#GPIO.add_event_detect(23, GPIO.RISING)
#GPIO.add_event_callback(23, charging)
#GPIO.add_event_detect(24, GPIO.RISING)
#GPIO.add_event_callback(24, charged)
#GPIO.add_event_detect(24, GPIO.RISING, callback=charged

#GPIO.add_event_detect(17, GPIO.FALLING)
#GPIO.add_event_callback(17, lowbattery)


while True:
	if not GPIO.input(23) and not GPIO.input(24):
	   print("Not plugged in")
	   
	if GPIO.input(23):
           charging(23)
	   #GPIO.add_event_detect(24, GPIO.RISING)
           #GPIO.add_event_callback(24, charged)

        if GPIO.input(24):
           charged(24)
	   #GPIO.add_event_detect(23, GPIO.RISING)
           #GPIO.add_event_callback(23, charging)
	
	if not GPIO.input(17):
           lowbattery(17)
           #GPIO.add_event_detect(23, GPIO.RISING)
           #GPIO.add_event_callback(23, charging)



	sleep(5)


GPIO.cleanup()
