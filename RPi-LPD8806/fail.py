#!/usr/bin/python

#from bootstrap import *

from time import sleep
from raspledstrip.ledstrip import *
from raspledstrip.animation import *

import os.path
import sys


num=8
led=LEDStrip(num)
led.all_off()


#setup colors to loop through for fade
colors = [
        (255.0,0.0,0.0),
#        (0.0,255.0,0.0),
#        (0.0,0.0,255.0),
#        (255.0,255.0,255.0),
]

step = 0.02
for x in range(2):
   for c in range(1):
           r, g, b = colors[c]
           level = 0.01
           dir = step
           while level >= 0.0:
                   led.fill(Color(r, g, b, level))
                   led.update()
                   if(level >= 0.99):
                           dir = -step
                   level += dir
                   #sleep(0.005)

led.all_off()

