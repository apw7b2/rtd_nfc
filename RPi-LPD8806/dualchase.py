#!/usr/bin/python

#from bootstrap import *

from time import sleep
from raspledstrip.ledstrip import *
from raspledstrip.animation import *

import os.path
import sys


num=8
led=LEDStrip(num)
led.all_off()

#setup colors for wipe and chase
colors = [
	Color(0, 0, 255),
#        Color(255, 0, 0),
#        Color(0, 0, 255),
        Color(255, 255, 255),
]

#for c in range(4):
#        anim = ColorWipe(led, colors[c])
#
#        for i in range(num):
#                anim.step()
#                led.update()
#                #sleep(0.03)
#
#led.fillOff()

dur=0.075
rounds=2
for c in range(2):
#        anim = ColorChase(led, colors[c])
#
        for i in range(rounds):
#                anim.step()
                led.update()
                led.set(0, colors[c])
		led.set(5, colors[c])
		led.update()
		sleep(dur)
		led.fillOff();
		led.set(1, colors[c])
		led.set(4, colors[c])
		led.update()
		sleep(dur)
		led.fillOff();
		led.set(2, colors[c])
		led.set(3, colors[c])
		led.update()
		sleep(dur)
		led.fillOff()
#led.fillOff()





led.all_off()


